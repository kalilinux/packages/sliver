#!/bin/sh

set -e

rm -f /tmp/local_127.0.0.1.cfg

expect << EOF &
spawn sliver-server
send "new-operator --name local --lhost 127.0.0.1 --save /tmp/local_127.0.0.1.cfg\n"
expect
send "multiplayer\n"
expect
EOF

until [ -f /tmp/local_127.0.0.1.cfg ]; do sleep 1; done

sliver-client import /tmp/local_127.0.0.1.cfg 2>/dev/null

expect << EOF
spawn sliver-client
expect "sliver >"
send "exit\n"
expect
EOF
